const http = require('http');
const database = require('./src/database')

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {
  res.statusCode = 200;
  res.setHeader('Content-Type', 'text/plain');
//   res.end('Hello World\n');


const result = {
    title : "Hier de titel",
    object : {test : "huh weer een test"},
    myArray : [{item1 : 12}]
};

// res.end(JSON.stringify(result));

database.movies.push(result);
res.end(JSON.stringify(database));

});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});

//curl http://127.0.0.1:3000

