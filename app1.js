console.log("Hello World!!!");
//alt shift f

let a = 10;

console.log("a = " + a);



function mijnFunction() {
    setTimeout(function () {
        return 12;
    }, 3000);
}

let b = mijnFunction();
console.log("b: ", b);

function mijnFunctionTwo(inputFunction) {
    setTimeout(function(){
        inputFunction(12);
    }, 3000);
}



let f = function (param) {
    console.log("param: ", param);
};

let result2 = mijnFunctionTwo(f);
console.log("finished earlier");

